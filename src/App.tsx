import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import { CartPage } from "pages/cart-page";
import { HomePage } from "pages/home-page";
import { ProductPage } from "pages/product-page";
import Header from "pages/componets/header";

const App = () => {
  return (
    <>
      <Router>
        <Header />
        <Switch>
          <Route exact path="/">
            <HomePage />
          </Route>
          <Route path="/product:id">
            <ProductPage />
          </Route>
          <Route path="/cart">
            <CartPage />
          </Route>
        </Switch>
      </Router>
    </>
  );
};

export default App;

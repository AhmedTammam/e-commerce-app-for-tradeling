import { configureStore } from "@reduxjs/toolkit";
import productsSlice from "pages/slices/product-slice";

export const store = configureStore({
  reducer: {
    products: productsSlice,
  },
});

export type RootState = ReturnType<typeof store.getState>;

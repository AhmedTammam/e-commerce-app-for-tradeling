import styled from "@emotion/styled";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { selectAllCartItems } from "pages/slices/product-slice";

const StyledHeaderWrapper = styled.div({
  height: 50,
  display: "flex",
  paddingRight: 40,
  alignItems: "center",
  justifyContent: "end",
  backgroundColor: "lightGray",
  a: {
    textDecoration: "none",
    backgroundColor: "blue",
    color: "white",
    padding: "10px 30px",
    borderRadius: 5,
  },
});

const Header = () => {
  const cartItems = useSelector(selectAllCartItems);
  return (
    <StyledHeaderWrapper>
      <NavLink to="/cart">
        {" "}
        Cart {!!cartItems.length && cartItems.length}
      </NavLink>
    </StyledHeaderWrapper>
  );
};

export default Header;

import styled from "@emotion/styled";
import { useDispatch } from "react-redux";
import { IProduct } from "types/product";
import { addToCart } from "pages/slices/product-slice";

const StyledBtn = styled.button({
  padding: 10,
  marginTop: 10,
  backgroundColor: "blue",
  color: "white",
  border: "none",
  borderRadius: 5,
  cursor: "pointer",
  display: "block",
});

const AddToCart = ({ product }: { product: IProduct }) => {
  const dispatch = useDispatch();

  return (
    <StyledBtn onClick={() => dispatch(addToCart(product))}>
      AddToCart
    </StyledBtn>
  );
};

export { AddToCart };

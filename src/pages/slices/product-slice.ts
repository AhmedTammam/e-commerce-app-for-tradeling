import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import { RootState } from "store";
import { IProduct } from "types/product";

interface IntialStateValues {
  isLoading: boolean;
  collection: IProduct[];
  cart: IProduct[];
}

export const initialState: IntialStateValues = {
  isLoading: false,
  collection: [],
  cart: [],
};

export const fetchAllProducts = createAsyncThunk(
  "fetch/all-products",
  async () => {
    try {
      const resp = await axios.get("https://fakestoreapi.com/products");
      console.log(resp);
      return resp.data;
    } catch (error) {
      console.log(error);
    }
  }
);

const productsSlice = createSlice({
  name: "products",
  initialState,
  reducers: {
    addToCart: (state, action) => {
      state.cart.push(action.payload);
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchAllProducts.pending, (state) => {
      state.isLoading = true;
    });
    builder.addCase(fetchAllProducts.fulfilled, (state, action) => {
      state.isLoading = false;
      state.collection = action.payload;
    });
  },
});

export const { addToCart } = productsSlice.actions;

export const selectAllProducts = (state: RootState) =>
  state.products.collection;

export const isLoadingStatus = (state: RootState) => state.products.isLoading;
export const selectProductById = (state: RootState, productId: string) =>
  state.products.collection.find(
    (product) => product.id === parseInt(productId)
  );

export const selectAllCartItems = (state: RootState) => state.products.cart;

export default productsSlice.reducer;

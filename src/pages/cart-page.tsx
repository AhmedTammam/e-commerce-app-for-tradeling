import styled from "@emotion/styled";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { StyledProductCardImg } from "./home-page/components/product-card";
import { selectAllCartItems } from "./slices/product-slice";

const StyledProductItem = styled.div({
  display: "flex",
  alignItems: "center",
  margin: "10px 20px",
  borderBottom: "1px solid black",
  paddingBottom: 20,
});

const StyledImg = styled(StyledProductCardImg)({});

const StyledProductInfo = styled.div({
  margin: "0 10px",
});

const CartPage = () => {
  const cartItems = useSelector(selectAllCartItems);
  return (
    <div>
      <h3>CartPage</h3>
      {cartItems.length ? (
        <div>
          {cartItems.map((item) => (
            <StyledProductItem key={item.id}>
              <StyledImg src={item.image} alt={item.title} />
              <StyledProductInfo>
                <p>{item.title}</p>
                <p>{item.description}</p>
              </StyledProductInfo>
              <strong>${item.price}</strong>
            </StyledProductItem>
          ))}
        </div>
      ) : (
        <p>There are not to items</p>
      )}
      <NavLink to="/"> Back to home</NavLink>
    </div>
  );
};

export { CartPage };

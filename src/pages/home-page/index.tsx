import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "@emotion/styled";
import ProductCard from "pages/home-page/components/product-card";
import {
  fetchAllProducts,
  isLoadingStatus,
  selectAllProducts,
} from "pages/slices/product-slice";

const StyledHomePageWrapper = styled.div({
  display: "flex",
  flexWrap: "wrap",
});

const HomePage = () => {
  const isLoading = useSelector(isLoadingStatus);
  const products = useSelector(selectAllProducts);

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchAllProducts());
  }, [dispatch]);

  if (isLoading) return <p>loading ...</p>;
  return (
    <StyledHomePageWrapper>
      {products.map((product) => (
        <ProductCard key={product.id} product={product} />
      ))}
    </StyledHomePageWrapper>
  );
};

export { HomePage };

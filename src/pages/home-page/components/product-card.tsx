import styled from "@emotion/styled";
import { useHistory } from "react-router";
import { IProduct } from "types/product";
import { AddToCart } from "pages/componets/add-to-cart";

export const StyledProductCard = styled.div({
  cursor: "pointer",
  border: "1px solid black",
  margin: 10,
  padding: 10,
  borderRadius: 5,
});

export const StyledProductCardImg = styled.img({
  height: 100,
});

const StyledProductInfoWrapper = styled.div({
  display: "flex",
  justifyContent: "space-between",
});

const ProductCard = ({ product }: { product: IProduct }) => {
  const { id, title, image, description, price } = product;

  const history = useHistory();
  return (
    <StyledProductCard style={{ maxWidth: "30%" }}>
      <div onClick={() => history.push(`/product${id}`)}>
        <StyledProductInfoWrapper>
          <div>
            <h4>{title}</h4>
            <strong>${price}</strong>
          </div>
          <StyledProductCardImg src={image} alt={title} />
        </StyledProductInfoWrapper>
        <p>{description}</p>
      </div>
      <AddToCart product={product} />
    </StyledProductCard>
  );
};

export default ProductCard;

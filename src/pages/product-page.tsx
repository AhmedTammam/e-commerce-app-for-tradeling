import styled from "@emotion/styled";
import { useSelector } from "react-redux";
import { useParams } from "react-router";
import { NavLink } from "react-router-dom";
import { RootState } from "store";
import { AddToCart } from "./componets/add-to-cart";
import { StyledProductCard } from "./home-page/components/product-card";
import { selectProductById } from "./slices/product-slice";

const StyledWrapper = styled.div({
  display: "flex",
  justifyContent: "space-between",
  div: {
    width: "50%",
  },
});

const StyledProductImgWrapper = styled.div({
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  height: "auto",
});
const StyledProductImg = styled.img({
  height: 200,
});

const ProductPage = () => {
  const { id }: { id: string } = useParams();
  const product = useSelector((state: RootState) =>
    selectProductById(state, id)
  );

  if (!product) return <p>SomeThing wrong happen</p>;

  return (
    <>
      <StyledWrapper>
        <StyledProductImgWrapper>
          <StyledProductImg src={product?.image} alt="product" />
        </StyledProductImgWrapper>
        <StyledProductCard>
          <h4>{product?.title}</h4>
          <p>{product?.description}</p>
          <strong>${product?.price}</strong>
          <AddToCart product={product} />
        </StyledProductCard>
      </StyledWrapper>
      <NavLink to="/"> Back to home</NavLink>
    </>
  );
};

export { ProductPage };

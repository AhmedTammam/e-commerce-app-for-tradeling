## Simple e-commerce

listing, productPage and cart page

## Technologies

---

- [React js](https://reactjs.org/)
- [Redux Toolkit](https://redux-toolkit.js.org/)
- [TypeScript](https://www.typescriptlang.org/)
- [Emotion js](https://emotion.sh/docs/introduction)

## How To Run

---

In the project directory, you can run:

`yarn start`
